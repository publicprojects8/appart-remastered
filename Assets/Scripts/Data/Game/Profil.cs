using UnityEngine;

public class Profil : MonoBehaviour
{
    public string Email { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string ScriptLocalPassWord { get; set; }

    public string FullName { get { return FirstName + " " + LastName; } }
}
