using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alarm
{
    
    public int Hour { get; set; }
    public int Min { get; set; }

}
