using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LinkedObjects
{
    public int Id { get; private set; }
    public string Name { get; set; }
}

public class LinkedPlug
{
    public double TotalConsumption { get; set;}

    public double AverageConsumption { get; set; }

    public bool State { get; set; }
}

public class LinkedLight
{
}
