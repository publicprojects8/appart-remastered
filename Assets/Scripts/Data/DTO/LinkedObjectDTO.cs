using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkedObjectDTO
{
    public long Id { get; set; }
    public string Name { get; set; }

    public string Location { get; set; }
}
