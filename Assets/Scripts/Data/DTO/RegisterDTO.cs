

public class RegisterDTO
{
    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Email { get; set; }

    public string PassWord { get; set; }

    public string PassWordVerification { get; set; }
}
