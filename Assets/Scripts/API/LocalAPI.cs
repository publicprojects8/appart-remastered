using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

/*public class LocalAPI : IAPI
{

    public IEnumerator Connect(ConnexionDTO dto, Action success, Action<string> error)
    {
        string destination = Application.persistentDataPath + "/" + dto.Email + "/profile.dat";
        FileStream file;

        if (File.Exists(destination))
        {
            error.Invoke("local file save not found");
            yield break;
        }

        file = File.OpenRead(destination);
        BinaryFormatter bf = new BinaryFormatter();
        Profil profil = (Profil)bf.Deserialize(file);

        file.Close();

        //ADD Encryption
        if(profil.ScriptLocalPassWord != dto.PassWord)
        {
            error.Invoke("password incorrect");
            yield break;
        }

        success.Invoke();
    }

    public IEnumerator Register(RegisterDTO dto, Action success, Action<string> error)
    {
        string destination = Application.persistentDataPath + "/" + dto.Email + "/profile.dat";
        FileStream file;

        if (Directory.Exists(Application.persistentDataPath + "/" + dto.Email) || File.Exists(destination))
        {
            error.Invoke("email already used");
            yield break;
        }

        Directory.CreateDirectory(Application.persistentDataPath + "/" + dto.Email);

        file = File.Create(destination);
        BinaryFormatter bf = new BinaryFormatter();
        Profil profil = new Profil { Email = dto.Email, FirstName = dto.FirstName, LastName = dto.LastName, ScriptLocalPassWord = dto.PassWord };

        bf.Serialize(file, profil);

        file.Close();

        success.Invoke();
    }
}*/
