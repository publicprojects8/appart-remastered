using System;
using System.Collections;
using System.Collections.Generic;

interface IAPI
{
    IEnumerator Connect(ConnexionDTO dto, Action success, Action<string> error);

    IEnumerator Register(RegisterDTO dto, Action success, Action<string> error);

    IEnumerator GetLinkedObjectsList(object dto, Action<List<LinkedObjectDTO>> success, Action<string> error);

    IEnumerator GetLinkedAlarmsList(object dto, Action success, Action<string> error);

    IEnumerator GetSingleLinkedObject(long linkedObjectID, Action<SingleLinkedObjectData> success, Action<string> error);

    IEnumerator GetSingleLinkedAlarm(object dto, Action success, Action<string> error);

    IEnumerator GetProfilData(object dto, Action success, Action<string> error);

}
