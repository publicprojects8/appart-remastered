using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FakeAPI : IAPI
{
    private FakeContext _context = new FakeContext();


    public IEnumerator Connect(ConnexionDTO dto, Action success, Action<string> error)
    {
        yield return new WaitForSeconds(2);

        success.Invoke();
    }

    public IEnumerator GetLinkedAlarmsList(object dto, Action success, Action<string> error)
    {
        yield return new WaitForSeconds(2);

        throw new NotImplementedException();
    }

    public IEnumerator GetLinkedObjectsList(object dto, Action<List<LinkedObjectDTO>> success, Action<string> error)
    {
        yield return new WaitForSeconds(2);

        List<LinkedObjectDTO> data = _context.user.LinkedObjects.Select(lo => new LinkedObjectDTO { Id = lo.Id, Name = lo.Name, Location = "" }).ToList();

        success.Invoke(data);
    }

    public IEnumerator GetProfilData(object dto, Action success, Action<string> error)
    {
        yield return new WaitForSeconds(2);

        throw new NotImplementedException();
    }

    public IEnumerator GetSingleLinkedAlarm(object dto, Action success, Action<string> error)
    {
        yield return new WaitForSeconds(2);

        throw new NotImplementedException();
    }

    public IEnumerator GetSingleLinkedObject(long linkedObjectID, Action<SingleLinkedObjectData> success, Action<string> error)
    {
        yield return new WaitForSeconds(2);

        SingleLinkedObjectData linkedObject = new SingleLinkedObjectData
        {
            LinkedObjectID = linkedObjectID,
            Name = _context.user.LinkedObjects.FirstOrDefault(lo => lo.Id == linkedObjectID).Name,
            LinkedObjectType = SingleLinkedObjectData.Type.Light
        };

        success.Invoke(linkedObject);
    }

    public IEnumerator Register(RegisterDTO dto, Action success, Action<string> error)
    {
        yield return new WaitForSeconds(2);

        success.Invoke();
    }
}



public class FakeContext
{
    public abstract class LinkedObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Light : LinkedObject
    {
    }

    public class User
    {
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Password { get; set; }

        public IEnumerable<LinkedObject> LinkedObjects { get; set; }
    }









    public User user = new User
    {
        Email = "jean-pierre@gmail.com",
        FirstName = "Jean-Pierre",
        LastName = "Dubois",
        Password = "123456",
        LinkedObjects = new List<LinkedObject>
        {
            new Light{ Id = 1, Name="Lampe Travail Bureau" },
            new Light{ Id = 2, Name="Lampe Chambre 1"},
            new Light{ Id = 2, Name="Lampe Chambre 2"}
        }
    };
}