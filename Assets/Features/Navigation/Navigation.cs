using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Navigation : MonoBehaviour
{
    [System.Serializable]
    public class Route
    {
        public string _id;
        public GameObject _page;
    }
    [SerializeField]
    public List<Route> _routes;

    public GameObject _parent;


    public enum ComponentChangeAnimation
    {
        Middle,
        Left,
        Right
    }

    public GameObject OpenNewPage(string route, ComponentChangeAnimation appearFrom = ComponentChangeAnimation.Middle)
    {
        Route routeData = _routes.FirstOrDefault(r => r._id == route);
        if (routeData == null) throw new System.Exception("");//A changer plus tard

        GameObject gameObject = Instantiate(routeData._page, _parent.transform);

        gameObject.GetComponent<Animator>().SetTrigger("Left");

        history.Add(gameObject);

        return gameObject;
    }



    public List<GameObject> history;

    IEnumerator Back()
    {
        if(history.Count != 0)
        {
            GameObject last_gm = history.Last();
            Animator animator = last_gm.GetComponent<Animator>();
            animator.SetTrigger("LeftInverse");
            yield return new WaitForSeconds(0.5f);
            history.Remove(last_gm);
            Destroy(last_gm);
        }
    }


    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(Back());
        }
    }
}
