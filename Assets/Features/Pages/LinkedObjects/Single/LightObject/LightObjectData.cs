using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightObjectData
{
    public double Intensity { get; set; }

    public string HexColor { get; set; }

    public bool On { get; set; }

    public LightObjectData()
    {
        Intensity = 100f;
        HexColor = "FFFFFF";
    }
}
