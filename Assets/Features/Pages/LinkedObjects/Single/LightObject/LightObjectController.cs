using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightObjectController : MonoBehaviour
{

    //Data
    private LightObjectData _data = new LightObjectData();


    //Components

    public Image img_bulb;

    public TMPro.TMP_Text txt_state;

    public Slider intensitySlider;

    public GameObject color_Container;

    public GameObject color_Selector_Prefab;

    public Material mat_bulb, mat_bulb_solid;


    public void Start()
    {
        Color[] colors =
        {
            new Color(1, 1, 1, 1),
            new Color(237/255f, 2/255f, 11/255f, 255/255f),
            new Color(244/255f, 101/255f, 40/255f, 255/255f),
            new Color(255/255f, 202/255f, 43/255f, 255/255f),
            new Color(238/255f, 47/255f, 127/255f, 255/255f),
            new Color(241/255f, 80/255f, 149/255f, 255/255f),
            new Color(255/255f, 160/255f, 206/255f, 255/255f),
            new Color(64/255f, 120/255f, 211/255f, 255/255f),
            new Color(80/255f, 168/255f, 227/255f, 255/255f),
            new Color(173/255f, 222/255f, 250/255f, 255/255f),
            new Color(127/255f, 176/255f, 5/255f, 255/255f),
            new Color(184/255f, 214/255f, 4/255f, 255/255f)
        };

        foreach (Color color in colors)
        {
            GameObject color_selector = Instantiate(color_Selector_Prefab, color_Container.transform);
            color_selector.GetComponent<Image>().color = color;
            color_selector.GetComponent<Button>().onClick.AddListener( delegate { ModifyColor(color); });
        }
    }

    public void ClickLight()
    {
        if (_data.On)
        {
            StopLight();
        }
        else
        {
            StartLight();
        }
    }

    public void StartLight()
    {
        Color color;
        ColorUtility.TryParseHtmlString(_data.HexColor, out color);

        img_bulb.color = Color32.Lerp(color, new Color(intensitySlider.value, intensitySlider.value, intensitySlider.value, 1), 0.5f);

        txt_state.text = "On";
        _data.On = true;
    }

    public void StopLight()
    {
        img_bulb.color = Color.black;
        txt_state.text = "Off";
        _data.On = false;
    }



    public void ModifyIntensity()
    {
        if (_data.On)
        {
            Color color;
            ColorUtility.TryParseHtmlString(_data.HexColor, out color);

            img_bulb.color = Color32.Lerp(color, new Color(intensitySlider.value, intensitySlider.value, intensitySlider.value, 1), 0.5f);
        }
    }

    public void ModifyColor(Color color)
    {
        if (_data.On)
        {
            img_bulb.color = Color32.Lerp(color, new Color(intensitySlider.value, intensitySlider.value, intensitySlider.value, 1), 0.5f);
        }
        Color32 color32 = color;
        _data.HexColor = "#" + color32.r.ToString("X2") + color32.g.ToString("X2") + color32.b.ToString("X2");
    }




    public void LoadData(LightObjectData data)
    {
        _data = data;

        intensitySlider.value = (float)data.Intensity;

        if (data.On)
        {
            txt_state.text = "On";
            img_bulb.material = mat_bulb;
        }
        else
        {
            txt_state.text = "Off";
            img_bulb.material = mat_bulb_solid;
        }

        Color color;
        ColorUtility.TryParseHtmlString(_data.HexColor, out color);

        img_bulb.color = Color32.Lerp(color, new Color(intensitySlider.value, intensitySlider.value, intensitySlider.value, 1), 0.5f);
    }

}
