using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleLinkedObjectData : MonoBehaviour
{
    public long LinkedObjectID { get; set; }

    public string Name { get; set; }

    public enum Type {
        Light
    }
    public Type LinkedObjectType { get; set; }
}
