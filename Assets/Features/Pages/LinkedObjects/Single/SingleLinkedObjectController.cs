using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class SingleLinkedObjectController : MonoBehaviour
{
    //Data
    private SingleLinkedObjectData _data;

    //Components
    public TMP_Text txt_Name;

    public GameObject body_socket;

    
    [System.Serializable]
    public class SingleLinkedObjectBody
    {
        public SingleLinkedObjectData.Type type;
        public GameObject prefab;
    }
    [SerializeField]
    public List<SingleLinkedObjectBody> children_prefabs;




    public void LoadData(SingleLinkedObjectData data)
    {
        _data = data;
        txt_Name.text = data.Name;
        SingleLinkedObjectBody prefab_Option = children_prefabs.FirstOrDefault(cp => cp.type == data.LinkedObjectType);
        GameObject gameObject = Instantiate(prefab_Option.prefab, body_socket.transform);

        switch (data.LinkedObjectType)
        {
            case SingleLinkedObjectData.Type.Light:
                LightObjectController controller = gameObject.GetComponent<LightObjectController>();
                controller.LoadData(new LightObjectData ());
                break;
        }

    }
}
