using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkedObjectsData
{
    public List<ElementLinkedObjectData> LinkedObjects { get; set; }
}
