using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkedObjectsController : MonoBehaviour
{
    //Data
    private LinkedObjectsData Data { get; set; }

    //Components
    public GameObject parent;

    public GameObject prefabElementsLinkedObject;

    private List<GameObject> ElementsLinkedObject { get; set; }

    public void LoadData(LinkedObjectsData _data)
    {
        ElementsLinkedObject = new List<GameObject>();
        foreach (ElementLinkedObjectData linkedObject in _data.LinkedObjects)
        {
            GameObject gameObject = Instantiate(prefabElementsLinkedObject, parent.transform);
            gameObject.GetComponent<ElementLinkedObjectController>().LoadData(linkedObject);
            ElementsLinkedObject.Add(gameObject);
        }
    }
}
