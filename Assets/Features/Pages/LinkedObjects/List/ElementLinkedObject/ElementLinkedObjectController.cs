using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ElementLinkedObjectController : MonoBehaviour
{

    //Data
    private ElementLinkedObjectData Data { get; set; }

    //Components
    public TextMeshProUGUI tmp_Name;
    public TextMeshProUGUI tmp_Location;

    public void LoadData(ElementLinkedObjectData data)
    {
        this.Data = data;
        tmp_Name.text = data.Name;
        tmp_Location.text = data.Location;
    }



    //Actions
    public void Select()
    {
        StartCoroutine(new FakeAPI().GetSingleLinkedObject(Data.LinkedObjectID, LoadSingleLinkedObject, null));
    }

    public void LoadSingleLinkedObject(SingleLinkedObjectData data)
    {
        Navigation navigation = GameObject.Find("Navigation").GetComponent<Navigation>();
        GameObject page = navigation.OpenNewPage("/LinkedObject", Navigation.ComponentChangeAnimation.Left);

        SingleLinkedObjectController controller = page.GetComponent<SingleLinkedObjectController>();
        controller.LoadData(data);
    }


}
