using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementLinkedObjectData
{
    public long LinkedObjectID { get; set; }
    public string Name { get; set; }
    public string Location { get; set; }
}
