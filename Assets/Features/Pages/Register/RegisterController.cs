using UnityEngine;
using UnityEngine.UI;

public class RegisterController : MonoBehaviour
{
    [SerializeField]
    public InputField FirstNameInput { get; set; }
    [SerializeField]
    public InputField LastNameInput { get; set; }
    [SerializeField]
    public InputField EmailInput { get; set; }
    [SerializeField]
    public InputField PasswordInput { get; set; }
    [SerializeField]
    public InputField PasswordVerificationInput { get; set; }


    public enum State
    {
        InProgress,
        Idle
    }
    public State state;

    public void TryRegister()
    {
        if (state == State.InProgress) return;
        if (!EmailInput.text.Contains('@'))
        {
            return;
        }
        if (!PasswordInput.text.Equals(PasswordVerificationInput.text))
        {
            return;
        }
        RegisterDTO dto = new RegisterDTO { FirstName = FirstNameInput.text, LastName = LastNameInput.text, Email = EmailInput.text, PassWord = PasswordInput.text };

        IAPI api = new FakeAPI();
        StartCoroutine(api.Register(dto, SuccessCallBack, ErrorCallBack));
    }

    public void SuccessCallBack()
    {

    }

    public void ErrorCallBack(string error)
    {

    }

    
}
