using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConnexionController : MonoBehaviour
{
    [SerializeField]
    public InputField EmailInput { get; set; }

    [SerializeField]
    public InputField PasswordInput { get; set; }


    public enum State
    {
        InProgress,
        Idle
    }
    public State state;

    public void TryConnect()
    {
        if (state == State.InProgress) return;
        if (!EmailInput.text.Contains('@'))
        {
            return;
        }
        ConnexionDTO dto = new ConnexionDTO { Email = EmailInput.text, PassWord = PasswordInput.text };

        IAPI api = new FakeAPI();
        StartCoroutine(api.Connect(dto, SuccessCallBack, ErrorCallBack));
    }

    public void SuccessCallBack()
    {

    }

    public void ErrorCallBack(string error)
    {

    }

    
}
