using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ProfilController
{
    //Data
    public ProfilData Data;

    //Component
    public TextMeshProUGUI InputFirstName;

    public TextMeshProUGUI InputLastName;

    public TextMeshProUGUI InputEmail;


    public void LoadData(ProfilData _data)
    {
        Data = _data;
        InputFirstName.text = _data.FirstName;
        InputLastName.text = _data.LastName;
        InputEmail.text = _data.Email;
    }
}
