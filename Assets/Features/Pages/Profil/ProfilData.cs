using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProfilData : MonoBehaviour
{
    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Email { get; set; }
}
