using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MenuController : MonoBehaviour
{

    //Data
    public enum State
    {
        None,
        Alarms,
        LinkedObjects,
        House,
        Profil
    }

    public State state;


    //Components
    public Transform parentPage;

    public GameObject alarmsPagePrefab;

    public GameObject linkedObjectPagePrefab;

    public GameObject housePagePrefab;

    public GameObject profilPagePrefab;


    public void GoToAlarmPage()
    {

    }  
    
    
    public void GoToLinkedObjectsPage()
    {
        StartCoroutine(new FakeAPI().GetLinkedObjectsList(null, ConstructLinkedObjectsPage, null));
    }


    public void ConstructLinkedObjectsPage(List<LinkedObjectDTO> linkedObjectDTO)
    {
        LinkedObjectsData data = new LinkedObjectsData 
            { 
                LinkedObjects = linkedObjectDTO
                                        .Select(dto => 
                                            new ElementLinkedObjectData 
                                                { 
                                                    LinkedObjectID = dto.Id, 
                                                    Name = dto.Name, 
                                                    Location = dto.Location 
                                                })
                                        .ToList() 
            };

        Navigation navigation = GameObject.Find("Navigation").GetComponent<Navigation>();
        GameObject page = navigation.OpenNewPage("/LinkedObjects", Calcul(State.LinkedObjects));

        LinkedObjectsController controller = page.GetComponent<LinkedObjectsController>();
        controller.LoadData(data);
        state = State.LinkedObjects;
    }




    private Navigation.ComponentChangeAnimation Calcul(State new_state)
    {
        return new_state == state || state == State.None ? Navigation.ComponentChangeAnimation.Middle : new_state > state ? Navigation.ComponentChangeAnimation.Right : Navigation.ComponentChangeAnimation.Left;
    }





    public void GoToHousePage()
    {

    }

    public void GoToProfilPage()
    {

    }
}
